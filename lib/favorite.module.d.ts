import * as i0 from "@angular/core";
import * as i1 from "./favorite.component";
import * as i2 from "@angular/common";
export declare class FavoriteModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<FavoriteModule, [typeof i1.FavoriteComponent], [typeof i2.CommonModule], [typeof i1.FavoriteComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<FavoriteModule>;
}
