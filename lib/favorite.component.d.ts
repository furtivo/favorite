import { EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
export declare class FavoriteComponent {
    isFavorite: boolean;
    favoritesCount: number;
    change: EventEmitter<any>;
    onClick(): void;
    static ɵfac: i0.ɵɵFactoryDef<FavoriteComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<FavoriteComponent, "lib-favorite", never, { "isFavorite": "is-favorite"; "favoritesCount": "favorites-count"; }, { "change": "change"; }, never, never>;
}
export interface FavoriteChangeEventArgs {
    newValue: boolean;
}
