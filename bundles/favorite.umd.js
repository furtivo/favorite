(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common')) :
    typeof define === 'function' && define.amd ? define('favorite', ['exports', '@angular/core', '@angular/common'], factory) :
    (global = global || self, factory(global.favorite = {}, global.ng.core, global.ng.common));
}(this, (function (exports, core, common) { 'use strict';

    var FavoriteService = /** @class */ (function () {
        function FavoriteService() {
        }
        FavoriteService.ɵfac = function FavoriteService_Factory(t) { return new (t || FavoriteService)(); };
        FavoriteService.ɵprov = core["ɵɵdefineInjectable"]({ token: FavoriteService, factory: FavoriteService.ɵfac, providedIn: 'root' });
        return FavoriteService;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FavoriteService, [{
            type: core.Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], function () { return []; }, null); })();

    function FavoriteComponent_i_1_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "i", 3);
        core["ɵɵtext"](1, "favorite");
        core["ɵɵelementEnd"]();
    } }
    function FavoriteComponent_i_2_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "i", 4);
        core["ɵɵtext"](1, "favorite");
        core["ɵɵelementEnd"]();
    } }
    var FavoriteComponent = /** @class */ (function () {
        function FavoriteComponent() {
            this.change = new core.EventEmitter();
        }
        FavoriteComponent.prototype.onClick = function () {
            this.isFavorite = !this.isFavorite;
            this.favoritesCount += (this.isFavorite) ? +1 : -1;
            this.change.emit({ newValue: this.isFavorite });
        };
        FavoriteComponent.ɵfac = function FavoriteComponent_Factory(t) { return new (t || FavoriteComponent)(); };
        FavoriteComponent.ɵcmp = core["ɵɵdefineComponent"]({ type: FavoriteComponent, selectors: [["lib-favorite"]], inputs: { isFavorite: ["is-favorite", "isFavorite"], favoritesCount: ["favorites-count", "favoritesCount"] }, outputs: { change: "change" }, decls: 5, vars: 3, consts: [[1, "favorite-container", 3, "click"], ["class", "material-icons favorite-active", 4, "ngIf"], ["class", "material-icons favorite", 4, "ngIf"], [1, "material-icons", "favorite-active"], [1, "material-icons", "favorite"]], template: function FavoriteComponent_Template(rf, ctx) { if (rf & 1) {
                core["ɵɵelementStart"](0, "span", 0);
                core["ɵɵlistener"]("click", function FavoriteComponent_Template_span_click_0_listener() { return ctx.onClick(); });
                core["ɵɵtemplate"](1, FavoriteComponent_i_1_Template, 2, 0, "i", 1);
                core["ɵɵtemplate"](2, FavoriteComponent_i_2_Template, 2, 0, "i", 2);
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](3, "span");
                core["ɵɵtext"](4);
                core["ɵɵelementEnd"]();
            } if (rf & 2) {
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("ngIf", ctx.isFavorite);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("ngIf", !ctx.isFavorite);
                core["ɵɵadvance"](2);
                core["ɵɵtextInterpolate"](ctx.favoritesCount);
            } }, directives: [common.NgIf], styles: [".favorite-container[_ngcontent-%COMP%] { vertical-align:middle;padding: 5px;  }", ".favorite-active[_ngcontent-%COMP%] { color: deeppink; cursor: pointer;  }", ".favorite[_ngcontent-%COMP%] {color: #cccccc;cursor: pointer;  }"] });
        return FavoriteComponent;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FavoriteComponent, [{
            type: core.Component,
            args: [{
                    selector: 'lib-favorite',
                    template: "  \n  <span (click)=\"onClick()\" class=\"favorite-container\" >\n  <i *ngIf=\"isFavorite\" class=\"material-icons favorite-active\">favorite</i>\n  <i *ngIf=\"!isFavorite\" class=\"material-icons favorite\">favorite</i>  \n  </span>  \n  <span>{{favoritesCount}}</span>\n  ",
                    styles: [
                        '.favorite-container { vertical-align:middle;padding: 5px;  }',
                        '.favorite-active { color: deeppink; cursor: pointer;  }',
                        ' .favorite {color: #cccccc;cursor: pointer;  }'
                    ],
                    encapsulation: core.ViewEncapsulation.Emulated
                }]
        }], null, { isFavorite: [{
                type: core.Input,
                args: ['is-favorite']
            }], favoritesCount: [{
                type: core.Input,
                args: ['favorites-count']
            }], change: [{
                type: core.Output
            }] }); })();

    var FavoriteModule = /** @class */ (function () {
        function FavoriteModule() {
        }
        FavoriteModule.ɵmod = core["ɵɵdefineNgModule"]({ type: FavoriteModule });
        FavoriteModule.ɵinj = core["ɵɵdefineInjector"]({ factory: function FavoriteModule_Factory(t) { return new (t || FavoriteModule)(); }, imports: [[common.CommonModule
                ]] });
        return FavoriteModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core["ɵɵsetNgModuleScope"](FavoriteModule, { declarations: [FavoriteComponent], imports: [common.CommonModule], exports: [FavoriteComponent] }); })();
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FavoriteModule, [{
            type: core.NgModule,
            args: [{
                    declarations: [FavoriteComponent],
                    imports: [common.CommonModule
                    ],
                    exports: [FavoriteComponent]
                }]
        }], null, null); })();

    exports.FavoriteComponent = FavoriteComponent;
    exports.FavoriteModule = FavoriteModule;
    exports.FavoriteService = FavoriteService;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=favorite.umd.js.map
