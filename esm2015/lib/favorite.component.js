import { Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
function FavoriteComponent_i_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "i", 3);
    i0.ɵɵtext(1, "favorite");
    i0.ɵɵelementEnd();
} }
function FavoriteComponent_i_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "i", 4);
    i0.ɵɵtext(1, "favorite");
    i0.ɵɵelementEnd();
} }
export class FavoriteComponent {
    constructor() {
        this.change = new EventEmitter();
    }
    onClick() {
        this.isFavorite = !this.isFavorite;
        this.favoritesCount += (this.isFavorite) ? +1 : -1;
        this.change.emit({ newValue: this.isFavorite });
    }
}
FavoriteComponent.ɵfac = function FavoriteComponent_Factory(t) { return new (t || FavoriteComponent)(); };
FavoriteComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FavoriteComponent, selectors: [["lib-favorite"]], inputs: { isFavorite: ["is-favorite", "isFavorite"], favoritesCount: ["favorites-count", "favoritesCount"] }, outputs: { change: "change" }, decls: 5, vars: 3, consts: [[1, "favorite-container", 3, "click"], ["class", "material-icons favorite-active", 4, "ngIf"], ["class", "material-icons favorite", 4, "ngIf"], [1, "material-icons", "favorite-active"], [1, "material-icons", "favorite"]], template: function FavoriteComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "span", 0);
        i0.ɵɵlistener("click", function FavoriteComponent_Template_span_click_0_listener() { return ctx.onClick(); });
        i0.ɵɵtemplate(1, FavoriteComponent_i_1_Template, 2, 0, "i", 1);
        i0.ɵɵtemplate(2, FavoriteComponent_i_2_Template, 2, 0, "i", 2);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(3, "span");
        i0.ɵɵtext(4);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.isFavorite);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", !ctx.isFavorite);
        i0.ɵɵadvance(2);
        i0.ɵɵtextInterpolate(ctx.favoritesCount);
    } }, directives: [i1.NgIf], styles: [".favorite-container[_ngcontent-%COMP%] { vertical-align:middle;padding: 5px;  }", ".favorite-active[_ngcontent-%COMP%] { color: deeppink; cursor: pointer;  }", ".favorite[_ngcontent-%COMP%] {color: #cccccc;cursor: pointer;  }"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FavoriteComponent, [{
        type: Component,
        args: [{
                selector: 'lib-favorite',
                template: `  
  <span (click)="onClick()" class="favorite-container" >
  <i *ngIf="isFavorite" class="material-icons favorite-active">favorite</i>
  <i *ngIf="!isFavorite" class="material-icons favorite">favorite</i>  
  </span>  
  <span>{{favoritesCount}}</span>
  `,
                styles: [
                    '.favorite-container { vertical-align:middle;padding: 5px;  }',
                    '.favorite-active { color: deeppink; cursor: pointer;  }',
                    ' .favorite {color: #cccccc;cursor: pointer;  }'
                ],
                encapsulation: ViewEncapsulation.Emulated
            }]
    }], null, { isFavorite: [{
            type: Input,
            args: ['is-favorite']
        }], favoritesCount: [{
            type: Input,
            args: ['favorites-count']
        }], change: [{
            type: Output
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmF2b3JpdGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vZmF2b3JpdGUvIiwic291cmNlcyI6WyJsaWIvZmF2b3JpdGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBQyxZQUFZLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7SUFPdkYsNEJBQTZEO0lBQUEsd0JBQVE7SUFBQSxpQkFBSTs7O0lBQ3pFLDRCQUF1RDtJQUFBLHdCQUFRO0lBQUEsaUJBQUk7O0FBWXJFLE1BQU0sT0FBTyxpQkFBaUI7SUFqQjlCO1FBcUJZLFdBQU0sR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO0tBUXZDO0lBTEMsT0FBTztRQUNMLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQ25DLElBQUksQ0FBQyxjQUFjLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUEsQ0FBQyxDQUFBLENBQUMsQ0FBQyxDQUFBLENBQUMsQ0FBQSxDQUFDLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLFFBQVEsRUFBQyxJQUFJLENBQUMsVUFBVSxFQUFDLENBQUMsQ0FBQztJQUNoRCxDQUFDOztrRkFYVSxpQkFBaUI7c0RBQWpCLGlCQUFpQjtRQWQ1QiwrQkFDQTtRQURNLDRGQUFTLGFBQVMsSUFBQztRQUN6Qiw4REFBNkQ7UUFDN0QsOERBQXVEO1FBQ3ZELGlCQUFPO1FBQ1AsNEJBQU07UUFBQSxZQUFrQjtRQUFBLGlCQUFPOztRQUg1QixlQUFrQjtRQUFsQixxQ0FBa0I7UUFDbEIsZUFBbUI7UUFBbkIsc0NBQW1CO1FBRWhCLGVBQWtCO1FBQWxCLHdDQUFrQjs7a0RBVWIsaUJBQWlCO2NBakI3QixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLGNBQWM7Z0JBQ3hCLFFBQVEsRUFBRTs7Ozs7O0dBTVQ7Z0JBQ0QsTUFBTSxFQUFFO29CQUNOLDhEQUE4RDtvQkFDOUQseURBQXlEO29CQUMzRCxnREFBZ0Q7aUJBQy9DO2dCQUNELGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxRQUFRO2FBRTFDOztrQkFHRSxLQUFLO21CQUFDLGFBQWE7O2tCQUNuQixLQUFLO21CQUFDLGlCQUFpQjs7a0JBQ3ZCLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPdXRwdXQsRXZlbnRFbWl0dGVyLCBWaWV3RW5jYXBzdWxhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2xpYi1mYXZvcml0ZScsXG4gIHRlbXBsYXRlOiBgICBcbiAgPHNwYW4gKGNsaWNrKT1cIm9uQ2xpY2soKVwiIGNsYXNzPVwiZmF2b3JpdGUtY29udGFpbmVyXCIgPlxuICA8aSAqbmdJZj1cImlzRmF2b3JpdGVcIiBjbGFzcz1cIm1hdGVyaWFsLWljb25zIGZhdm9yaXRlLWFjdGl2ZVwiPmZhdm9yaXRlPC9pPlxuICA8aSAqbmdJZj1cIiFpc0Zhdm9yaXRlXCIgY2xhc3M9XCJtYXRlcmlhbC1pY29ucyBmYXZvcml0ZVwiPmZhdm9yaXRlPC9pPiAgXG4gIDwvc3Bhbj4gIFxuICA8c3Bhbj57e2Zhdm9yaXRlc0NvdW50fX08L3NwYW4+XG4gIGAsXG4gIHN0eWxlczogW1xuICAgICcuZmF2b3JpdGUtY29udGFpbmVyIHsgdmVydGljYWwtYWxpZ246bWlkZGxlO3BhZGRpbmc6IDVweDsgIH0nLCAgXG4gICAgJy5mYXZvcml0ZS1hY3RpdmUgeyBjb2xvcjogZGVlcHBpbms7IGN1cnNvcjogcG9pbnRlcjsgIH0nLFxuICAnIC5mYXZvcml0ZSB7Y29sb3I6ICNjY2NjY2M7Y3Vyc29yOiBwb2ludGVyOyAgfScgXG4gIF0sXG4gIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLkVtdWxhdGVkXG4gIFxufSlcbmV4cG9ydCBjbGFzcyBGYXZvcml0ZUNvbXBvbmVudCB7XG5cbiAgQElucHV0KCdpcy1mYXZvcml0ZScpIGlzRmF2b3JpdGU6IGJvb2xlYW47XG4gIEBJbnB1dCgnZmF2b3JpdGVzLWNvdW50JykgZmF2b3JpdGVzQ291bnQ6IG51bWJlcjtcbiAgQE91dHB1dCgpIGNoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuXG4gIG9uQ2xpY2soKSB7XG4gICAgdGhpcy5pc0Zhdm9yaXRlID0gIXRoaXMuaXNGYXZvcml0ZTtcbiAgICB0aGlzLmZhdm9yaXRlc0NvdW50ICs9ICh0aGlzLmlzRmF2b3JpdGUpPysxOi0xO1xuICAgIHRoaXMuY2hhbmdlLmVtaXQoeyBuZXdWYWx1ZTp0aGlzLmlzRmF2b3JpdGV9KTtcbiAgfVxufVxuXG5leHBvcnQgaW50ZXJmYWNlIEZhdm9yaXRlQ2hhbmdlRXZlbnRBcmdzIHtcbiAgbmV3VmFsdWU6IGJvb2xlYW5cbn1cblxuIl19