import { NgModule } from '@angular/core';
import { FavoriteComponent } from './favorite.component';
import { CommonModule } from '@angular/common';
import * as i0 from "@angular/core";
export class FavoriteModule {
}
FavoriteModule.ɵmod = i0.ɵɵdefineNgModule({ type: FavoriteModule });
FavoriteModule.ɵinj = i0.ɵɵdefineInjector({ factory: function FavoriteModule_Factory(t) { return new (t || FavoriteModule)(); }, imports: [[CommonModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(FavoriteModule, { declarations: [FavoriteComponent], imports: [CommonModule], exports: [FavoriteComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FavoriteModule, [{
        type: NgModule,
        args: [{
                declarations: [FavoriteComponent],
                imports: [CommonModule
                ],
                exports: [FavoriteComponent]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmF2b3JpdGUubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZmF2b3JpdGUvIiwic291cmNlcyI6WyJsaWIvZmF2b3JpdGUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFFekQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDOztBQVEvQyxNQUFNLE9BQU8sY0FBYzs7a0RBQWQsY0FBYzsyR0FBZCxjQUFjLGtCQUpoQixDQUFDLFlBQVk7U0FDckI7d0ZBR1UsY0FBYyxtQkFMVixpQkFBaUIsYUFDdEIsWUFBWSxhQUVaLGlCQUFpQjtrREFFaEIsY0FBYztjQU4xQixRQUFRO2VBQUM7Z0JBQ1IsWUFBWSxFQUFFLENBQUMsaUJBQWlCLENBQUM7Z0JBQ2pDLE9BQU8sRUFBRSxDQUFDLFlBQVk7aUJBQ3JCO2dCQUNELE9BQU8sRUFBRSxDQUFDLGlCQUFpQixDQUFDO2FBQzdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZhdm9yaXRlQ29tcG9uZW50IH0gZnJvbSAnLi9mYXZvcml0ZS5jb21wb25lbnQnO1xuXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nOyAgXG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW0Zhdm9yaXRlQ29tcG9uZW50XSxcbiAgaW1wb3J0czogW0NvbW1vbk1vZHVsZVxuICBdLFxuICBleHBvcnRzOiBbRmF2b3JpdGVDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIEZhdm9yaXRlTW9kdWxlIHsgfVxuIl19