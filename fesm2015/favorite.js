import { ɵɵdefineInjectable, ɵsetClassMetadata, Injectable, ɵɵelementStart, ɵɵtext, ɵɵelementEnd, EventEmitter, ɵɵdefineComponent, ɵɵlistener, ɵɵtemplate, ɵɵadvance, ɵɵproperty, ɵɵtextInterpolate, Component, ViewEncapsulation, Input, Output, ɵɵdefineNgModule, ɵɵdefineInjector, ɵɵsetNgModuleScope, NgModule } from '@angular/core';
import { NgIf, CommonModule } from '@angular/common';

class FavoriteService {
    constructor() { }
}
FavoriteService.ɵfac = function FavoriteService_Factory(t) { return new (t || FavoriteService)(); };
FavoriteService.ɵprov = ɵɵdefineInjectable({ token: FavoriteService, factory: FavoriteService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { ɵsetClassMetadata(FavoriteService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();

function FavoriteComponent_i_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "i", 3);
    ɵɵtext(1, "favorite");
    ɵɵelementEnd();
} }
function FavoriteComponent_i_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "i", 4);
    ɵɵtext(1, "favorite");
    ɵɵelementEnd();
} }
class FavoriteComponent {
    constructor() {
        this.change = new EventEmitter();
    }
    onClick() {
        this.isFavorite = !this.isFavorite;
        this.favoritesCount += (this.isFavorite) ? +1 : -1;
        this.change.emit({ newValue: this.isFavorite });
    }
}
FavoriteComponent.ɵfac = function FavoriteComponent_Factory(t) { return new (t || FavoriteComponent)(); };
FavoriteComponent.ɵcmp = ɵɵdefineComponent({ type: FavoriteComponent, selectors: [["lib-favorite"]], inputs: { isFavorite: ["is-favorite", "isFavorite"], favoritesCount: ["favorites-count", "favoritesCount"] }, outputs: { change: "change" }, decls: 5, vars: 3, consts: [[1, "favorite-container", 3, "click"], ["class", "material-icons favorite-active", 4, "ngIf"], ["class", "material-icons favorite", 4, "ngIf"], [1, "material-icons", "favorite-active"], [1, "material-icons", "favorite"]], template: function FavoriteComponent_Template(rf, ctx) { if (rf & 1) {
        ɵɵelementStart(0, "span", 0);
        ɵɵlistener("click", function FavoriteComponent_Template_span_click_0_listener() { return ctx.onClick(); });
        ɵɵtemplate(1, FavoriteComponent_i_1_Template, 2, 0, "i", 1);
        ɵɵtemplate(2, FavoriteComponent_i_2_Template, 2, 0, "i", 2);
        ɵɵelementEnd();
        ɵɵelementStart(3, "span");
        ɵɵtext(4);
        ɵɵelementEnd();
    } if (rf & 2) {
        ɵɵadvance(1);
        ɵɵproperty("ngIf", ctx.isFavorite);
        ɵɵadvance(1);
        ɵɵproperty("ngIf", !ctx.isFavorite);
        ɵɵadvance(2);
        ɵɵtextInterpolate(ctx.favoritesCount);
    } }, directives: [NgIf], styles: [".favorite-container[_ngcontent-%COMP%] { vertical-align:middle;padding: 5px;  }", ".favorite-active[_ngcontent-%COMP%] { color: deeppink; cursor: pointer;  }", ".favorite[_ngcontent-%COMP%] {color: #cccccc;cursor: pointer;  }"] });
/*@__PURE__*/ (function () { ɵsetClassMetadata(FavoriteComponent, [{
        type: Component,
        args: [{
                selector: 'lib-favorite',
                template: `  
  <span (click)="onClick()" class="favorite-container" >
  <i *ngIf="isFavorite" class="material-icons favorite-active">favorite</i>
  <i *ngIf="!isFavorite" class="material-icons favorite">favorite</i>  
  </span>  
  <span>{{favoritesCount}}</span>
  `,
                styles: [
                    '.favorite-container { vertical-align:middle;padding: 5px;  }',
                    '.favorite-active { color: deeppink; cursor: pointer;  }',
                    ' .favorite {color: #cccccc;cursor: pointer;  }'
                ],
                encapsulation: ViewEncapsulation.Emulated
            }]
    }], null, { isFavorite: [{
            type: Input,
            args: ['is-favorite']
        }], favoritesCount: [{
            type: Input,
            args: ['favorites-count']
        }], change: [{
            type: Output
        }] }); })();

class FavoriteModule {
}
FavoriteModule.ɵmod = ɵɵdefineNgModule({ type: FavoriteModule });
FavoriteModule.ɵinj = ɵɵdefineInjector({ factory: function FavoriteModule_Factory(t) { return new (t || FavoriteModule)(); }, imports: [[CommonModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(FavoriteModule, { declarations: [FavoriteComponent], imports: [CommonModule], exports: [FavoriteComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(FavoriteModule, [{
        type: NgModule,
        args: [{
                declarations: [FavoriteComponent],
                imports: [CommonModule
                ],
                exports: [FavoriteComponent]
            }]
    }], null, null); })();

/*
 * Public API Surface of favorite
 */

/**
 * Generated bundle index. Do not edit.
 */

export { FavoriteComponent, FavoriteModule, FavoriteService };
//# sourceMappingURL=favorite.js.map
