import { NgModule } from '@angular/core';
import { FavoriteComponent } from './favorite.component';
import { CommonModule } from '@angular/common';
import * as i0 from "@angular/core";
var FavoriteModule = /** @class */ (function () {
    function FavoriteModule() {
    }
    FavoriteModule.ɵmod = i0.ɵɵdefineNgModule({ type: FavoriteModule });
    FavoriteModule.ɵinj = i0.ɵɵdefineInjector({ factory: function FavoriteModule_Factory(t) { return new (t || FavoriteModule)(); }, imports: [[CommonModule
            ]] });
    return FavoriteModule;
}());
export { FavoriteModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(FavoriteModule, { declarations: [FavoriteComponent], imports: [CommonModule], exports: [FavoriteComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FavoriteModule, [{
        type: NgModule,
        args: [{
                declarations: [FavoriteComponent],
                imports: [CommonModule
                ],
                exports: [FavoriteComponent]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmF2b3JpdGUubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZmF2b3JpdGUvIiwic291cmNlcyI6WyJsaWIvZmF2b3JpdGUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFFekQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDOztBQUUvQztJQUFBO0tBTStCO3NEQUFsQixjQUFjOytHQUFkLGNBQWMsa0JBSmhCLENBQUMsWUFBWTthQUNyQjt5QkFSSDtDQVcrQixBQU4vQixJQU0rQjtTQUFsQixjQUFjO3dGQUFkLGNBQWMsbUJBTFYsaUJBQWlCLGFBQ3RCLFlBQVksYUFFWixpQkFBaUI7a0RBRWhCLGNBQWM7Y0FOMUIsUUFBUTtlQUFDO2dCQUNSLFlBQVksRUFBRSxDQUFDLGlCQUFpQixDQUFDO2dCQUNqQyxPQUFPLEVBQUUsQ0FBQyxZQUFZO2lCQUNyQjtnQkFDRCxPQUFPLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQzthQUM3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGYXZvcml0ZUNvbXBvbmVudCB9IGZyb20gJy4vZmF2b3JpdGUuY29tcG9uZW50JztcblxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJzsgIFxuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtGYXZvcml0ZUNvbXBvbmVudF0sXG4gIGltcG9ydHM6IFtDb21tb25Nb2R1bGVcbiAgXSxcbiAgZXhwb3J0czogW0Zhdm9yaXRlQ29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBGYXZvcml0ZU1vZHVsZSB7IH1cbiJdfQ==