import { Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
function FavoriteComponent_i_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "i", 3);
    i0.ɵɵtext(1, "favorite");
    i0.ɵɵelementEnd();
} }
function FavoriteComponent_i_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "i", 4);
    i0.ɵɵtext(1, "favorite");
    i0.ɵɵelementEnd();
} }
var FavoriteComponent = /** @class */ (function () {
    function FavoriteComponent() {
        this.change = new EventEmitter();
    }
    FavoriteComponent.prototype.onClick = function () {
        this.isFavorite = !this.isFavorite;
        this.favoritesCount += (this.isFavorite) ? +1 : -1;
        this.change.emit({ newValue: this.isFavorite });
    };
    FavoriteComponent.ɵfac = function FavoriteComponent_Factory(t) { return new (t || FavoriteComponent)(); };
    FavoriteComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FavoriteComponent, selectors: [["lib-favorite"]], inputs: { isFavorite: ["is-favorite", "isFavorite"], favoritesCount: ["favorites-count", "favoritesCount"] }, outputs: { change: "change" }, decls: 5, vars: 3, consts: [[1, "favorite-container", 3, "click"], ["class", "material-icons favorite-active", 4, "ngIf"], ["class", "material-icons favorite", 4, "ngIf"], [1, "material-icons", "favorite-active"], [1, "material-icons", "favorite"]], template: function FavoriteComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "span", 0);
            i0.ɵɵlistener("click", function FavoriteComponent_Template_span_click_0_listener() { return ctx.onClick(); });
            i0.ɵɵtemplate(1, FavoriteComponent_i_1_Template, 2, 0, "i", 1);
            i0.ɵɵtemplate(2, FavoriteComponent_i_2_Template, 2, 0, "i", 2);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(3, "span");
            i0.ɵɵtext(4);
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.isFavorite);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !ctx.isFavorite);
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate(ctx.favoritesCount);
        } }, directives: [i1.NgIf], styles: [".favorite-container[_ngcontent-%COMP%] { vertical-align:middle;padding: 5px;  }", ".favorite-active[_ngcontent-%COMP%] { color: deeppink; cursor: pointer;  }", ".favorite[_ngcontent-%COMP%] {color: #cccccc;cursor: pointer;  }"] });
    return FavoriteComponent;
}());
export { FavoriteComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FavoriteComponent, [{
        type: Component,
        args: [{
                selector: 'lib-favorite',
                template: "  \n  <span (click)=\"onClick()\" class=\"favorite-container\" >\n  <i *ngIf=\"isFavorite\" class=\"material-icons favorite-active\">favorite</i>\n  <i *ngIf=\"!isFavorite\" class=\"material-icons favorite\">favorite</i>  \n  </span>  \n  <span>{{favoritesCount}}</span>\n  ",
                styles: [
                    '.favorite-container { vertical-align:middle;padding: 5px;  }',
                    '.favorite-active { color: deeppink; cursor: pointer;  }',
                    ' .favorite {color: #cccccc;cursor: pointer;  }'
                ],
                encapsulation: ViewEncapsulation.Emulated
            }]
    }], null, { isFavorite: [{
            type: Input,
            args: ['is-favorite']
        }], favoritesCount: [{
            type: Input,
            args: ['favorites-count']
        }], change: [{
            type: Output
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmF2b3JpdGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vZmF2b3JpdGUvIiwic291cmNlcyI6WyJsaWIvZmF2b3JpdGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBQyxZQUFZLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7SUFPdkYsNEJBQTZEO0lBQUEsd0JBQVE7SUFBQSxpQkFBSTs7O0lBQ3pFLDRCQUF1RDtJQUFBLHdCQUFRO0lBQUEsaUJBQUk7O0FBTHJFO0lBQUE7UUFxQlksV0FBTSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7S0FRdkM7SUFMQyxtQ0FBTyxHQUFQO1FBQ0UsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDbkMsSUFBSSxDQUFDLGNBQWMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQSxDQUFDLENBQUEsQ0FBQyxDQUFDLENBQUEsQ0FBQyxDQUFBLENBQUMsQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsUUFBUSxFQUFDLElBQUksQ0FBQyxVQUFVLEVBQUMsQ0FBQyxDQUFDO0lBQ2hELENBQUM7c0ZBWFUsaUJBQWlCOzBEQUFqQixpQkFBaUI7WUFkNUIsK0JBQ0E7WUFETSw0RkFBUyxhQUFTLElBQUM7WUFDekIsOERBQTZEO1lBQzdELDhEQUF1RDtZQUN2RCxpQkFBTztZQUNQLDRCQUFNO1lBQUEsWUFBa0I7WUFBQSxpQkFBTzs7WUFINUIsZUFBa0I7WUFBbEIscUNBQWtCO1lBQ2xCLGVBQW1CO1lBQW5CLHNDQUFtQjtZQUVoQixlQUFrQjtZQUFsQix3Q0FBa0I7OzRCQVYxQjtDQWdDQyxBQTdCRCxJQTZCQztTQVpZLGlCQUFpQjtrREFBakIsaUJBQWlCO2NBakI3QixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLGNBQWM7Z0JBQ3hCLFFBQVEsRUFBRSxvUkFNVDtnQkFDRCxNQUFNLEVBQUU7b0JBQ04sOERBQThEO29CQUM5RCx5REFBeUQ7b0JBQzNELGdEQUFnRDtpQkFDL0M7Z0JBQ0QsYUFBYSxFQUFFLGlCQUFpQixDQUFDLFFBQVE7YUFFMUM7O2tCQUdFLEtBQUs7bUJBQUMsYUFBYTs7a0JBQ25CLEtBQUs7bUJBQUMsaUJBQWlCOztrQkFDdkIsTUFBTSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE91dHB1dCxFdmVudEVtaXR0ZXIsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbGliLWZhdm9yaXRlJyxcbiAgdGVtcGxhdGU6IGAgIFxuICA8c3BhbiAoY2xpY2spPVwib25DbGljaygpXCIgY2xhc3M9XCJmYXZvcml0ZS1jb250YWluZXJcIiA+XG4gIDxpICpuZ0lmPVwiaXNGYXZvcml0ZVwiIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnMgZmF2b3JpdGUtYWN0aXZlXCI+ZmF2b3JpdGU8L2k+XG4gIDxpICpuZ0lmPVwiIWlzRmF2b3JpdGVcIiBjbGFzcz1cIm1hdGVyaWFsLWljb25zIGZhdm9yaXRlXCI+ZmF2b3JpdGU8L2k+ICBcbiAgPC9zcGFuPiAgXG4gIDxzcGFuPnt7ZmF2b3JpdGVzQ291bnR9fTwvc3Bhbj5cbiAgYCxcbiAgc3R5bGVzOiBbXG4gICAgJy5mYXZvcml0ZS1jb250YWluZXIgeyB2ZXJ0aWNhbC1hbGlnbjptaWRkbGU7cGFkZGluZzogNXB4OyAgfScsICBcbiAgICAnLmZhdm9yaXRlLWFjdGl2ZSB7IGNvbG9yOiBkZWVwcGluazsgY3Vyc29yOiBwb2ludGVyOyAgfScsXG4gICcgLmZhdm9yaXRlIHtjb2xvcjogI2NjY2NjYztjdXJzb3I6IHBvaW50ZXI7ICB9JyBcbiAgXSxcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uRW11bGF0ZWRcbiAgXG59KVxuZXhwb3J0IGNsYXNzIEZhdm9yaXRlQ29tcG9uZW50IHtcblxuICBASW5wdXQoJ2lzLWZhdm9yaXRlJykgaXNGYXZvcml0ZTogYm9vbGVhbjtcbiAgQElucHV0KCdmYXZvcml0ZXMtY291bnQnKSBmYXZvcml0ZXNDb3VudDogbnVtYmVyO1xuICBAT3V0cHV0KCkgY2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG5cbiAgb25DbGljaygpIHtcbiAgICB0aGlzLmlzRmF2b3JpdGUgPSAhdGhpcy5pc0Zhdm9yaXRlO1xuICAgIHRoaXMuZmF2b3JpdGVzQ291bnQgKz0gKHRoaXMuaXNGYXZvcml0ZSk/KzE6LTE7XG4gICAgdGhpcy5jaGFuZ2UuZW1pdCh7IG5ld1ZhbHVlOnRoaXMuaXNGYXZvcml0ZX0pO1xuICB9XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgRmF2b3JpdGVDaGFuZ2VFdmVudEFyZ3Mge1xuICBuZXdWYWx1ZTogYm9vbGVhblxufVxuXG4iXX0=